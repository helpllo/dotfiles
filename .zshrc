# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Variables
export ZSH="$HOME/.oh-my-zsh" # Path to your oh-my-zsh installation.
export EDITOR="nvim" # neovim is the best!
export PATH="$PATH:/home/linuxbrew/.linuxbrew/bin" # For homebrew installs
# My aliases
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
alias upall='sudo apt update && sudo apt upgrade -y && flatpak update -y && brew update && brew upgrade'
# Plugins
plugins=(git)

# My sources (export to ZSH)
source ~/.zsh/powerlevel10k/powerlevel10k.zsh-theme # theme
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh # suggestions
source $ZSH/oh-my-zsh.sh
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh # For powerlevel10k
export HISTSIZE=500

if [ -e /home/linuxmarek/.nix-profile/etc/profile.d/nix.sh ]; then . /home/linuxmarek/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer
