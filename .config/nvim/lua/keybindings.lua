local function map(m, k, v)
    vim.keymap.set(m,k,v,{silent = true})
end

vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Load recent essions
--map("n", "<leader>sl", "<CMD>SessionLoad<CR>")

-- Keybindings for buffers managent
map("n", "<leader>bd", "<CMD>bdelete<CR>")
map("n", "<leader>bf", "<CMD>bdelete!<CR>")
map("n", "<leader>bj", "<CMD>bnext<CR>")
map("n", "<leader>bk", "<CMD>bprevious<CR>")

-- Keybindings for finding some stuff
map("n", "<leader>fr", "<CMD>Telescope oldfiles<CR>")
map("n", "<leader>ft", "<CMD>Telescope colorscheme<CR>")
map("n", "<leader>ff", "<CMD>Telescope file_browser path=$HOME<CR>")
map("n", "<leader>fg", "<CMD>Telescope live_grep<CR>")
map("n", "<leader>.", "<CMD>Telescope file_browser<CR>")
map("n", "<leader>,", "<CMD>Telescope buffers<CR>")
map("n", "<leader>/", "<CMD>Telescope file_browser path=%:h<CR>")

-- For code development
map("n", "<leader>cc", "<CMD>!make <CR>")
map("n", "<leader>ce", "<CMD>!make run<CR>")
map("n", "<leader>ct", "<CMD>vsp<CR><CMD>terminal<CR>")
map("n", "<leader>cn", function() vim.lsp.buf.rename() end )
map("n", "<leader>cr", function() vim.lsp.buf.references() end )
map("n", "<leader>cd", function() vim.lsp.buf.definition() end )
--map("v", "<leader>cy", function() vim.lsp.buf.code_action() end )

-- For mangenting tabs
map("n", "<leader>xx", "<CMD>tabnew<CR>")
map("n", "<leader>xd", "<CMD>tabclose<CR>")
map("n", "<leader>xn", "<CMD>tabnext<CR>")
map("n", "<leader>xp", "<CMD>tabprevious<CR>")

-- Other stuff
map("n", "<leader>n", "<CMD>noh<CR>")
map("n", "<C-h>", "<C-w>h")
map("n", "<C-j>", "<C-w>j")
map("n", "<C-k>", "<C-w>k")
map("n", "<C-l>", "<C-w>l")

-- For terminal management
map("t", "<ESC>", [[<C-\><C-n>]])
map("t", "<ESC>", "<ESC><CMD>:ToggleTerm<CR>")
map("n", "<leader>t", "<CMD>ToggleTerm direction=tab<CR>")
