local o = vim.o

-- Basic settings
o.termguicolors = true
o.number = true
o.nonumber = false
o.relativenumber = false
-- Theme
o.background = "dark"
vim.cmd([[colorscheme gruvbox]])
--vim.cmd([[let g:omnisharp_server_use_mono = 1]])
-- UX
o.tabstop = 4
o.shiftwidth = 4
o.expandtab = true
o.clipboard = "unnamedplus"
o.mousemodel = "extend"
o.shell = "zsh"
-- Font
vim.opt.guifont = {"Fira Code",":h12"}
