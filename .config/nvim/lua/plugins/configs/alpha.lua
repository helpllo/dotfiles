
local alpha = require('alpha')
local dashboard = require('alpha.themes.dashboard')
local file = require('nvim-web-devicons').get_icon_


dashboard.section.header.val = {
    [[]],
    [[ ███╗   ██╗ ███████╗ ██████╗  ██╗   ██╗ ██╗ ███╗   ███╗ ]],
    [[ ████╗  ██║ ██╔════╝██╔═══██╗ ██║   ██║ ██║ ████╗ ████║ ]],
    [[ ██╔██╗ ██║ █████╗  ██║   ██║ ██║   ██║ ██║ ██╔████╔██║ ]],
    [[ ██║╚██╗██║ ██╔══╝  ██║   ██║ ╚██╗ ██╔╝ ██║ ██║╚██╔╝██║ ]],
    [[ ██║ ╚████║ ███████╗╚██████╔╝  ╚████╔╝  ██║ ██║ ╚═╝ ██║ ]],
    [[ ╚═╝  ╚═══╝ ╚══════╝ ╚═════╝    ╚═══╝   ╚═╝ ╚═╝     ╚═╝ ]],
    [[]],
    [[             [NOTE] If you can, write code              ]],
}

dashboard.section.buttons.val = {
    dashboard.button("SPC q q", "🗄️ Run away", ":qa<CR>"),
    dashboard.button("SPC f f", "📁 Open file browser", ":Telescope file_browser<CR>"),
    dashboard.button("SPC f r", "⟲ Open recent files", ":Telescope oldfiles<CR>"),
    dashboard.button("SPC ,", "Open buffers selection", ":Telescope buffers<CR>"),
}

dashboard.section.footer.val = "I will burn my computer if Neovim feels slow."

alpha.setup(dashboard.config)
--local status, db = pcall(require, "dashboard")
--local dbe = require("dashboard")

--dbe.default_banner = {
--	"",
--	" ███╗   ██╗ ███████╗ ██████╗  ██╗   ██╗ ██╗ ███╗   ███╗",
--	" ████╗  ██║ ██╔════╝██╔═══██╗ ██║   ██║ ██║ ████╗ ████║",
--	" ██╔██╗ ██║ █████╗  ██║   ██║ ██║   ██║ ██║ ██╔████╔██║",
--	" ██║╚██╗██║ ██╔══╝  ██║   ██║ ╚██╗ ██╔╝ ██║ ██║╚██╔╝██║",
--	" ██║ ╚████║ ███████╗╚██████╔╝  ╚████╔╝  ██║ ██║ ╚═╝ ██║",
--	" ╚═╝  ╚═══╝ ╚══════╝ ╚═════╝    ╚═══╝   ╚═╝ ╚═╝     ╚═╝",
--	"",
--	"        	  [NOTE] If you can, write code             ",
--}
--dbe.custom_center = {
--    {desc = 'Open file browser ', action = 'Telescope file_browser', shortcut = 'SPC .'},
--    {desc = 'Switch buffers ', action = 'Telescope buffers', shortcut = 'SPC ,'},
 --   {desc = 'Open recent files ', action = 'Telescope oldfiles', shortcut = 'SPC f r'},
--    {desc = 'Open recent session ', action = 'SessionLoad', shortcut = 'SPC s l'},
--}
--dbe.custom_footer = {"","I will burn my computer if Neovim feels slow."}
