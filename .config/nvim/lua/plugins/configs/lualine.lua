local options = {
    options = {
        icons_enabled = true,
        theme = gruvbox,   
    },
    sections = {
        lualine_a = {'mode'},
        lualine_b = {'branch', 'diff'},
        lualine_c = {'filename'},
        lualine_x = {'fileformat'},
        lualine_y = {'filetype'},
        lualine_z = {'location'},
    }
}
return options
