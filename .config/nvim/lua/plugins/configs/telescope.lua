--[[local status, telescope = pcall(require, "telescope")
if not status then 
		return 
end]]
local options = {
    extensions = {
        file_browser = {
            hidden = {folder_browser = true, file_browser = true}  
        }
    };
    picker = {
        file_browser = {
            path = "%:p:h"
        }
    }
}
return options
