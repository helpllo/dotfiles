--require('nvim-treesitter.configs').setup 
local options = {
    highlight = {
        enable = true,
        additional_vim_regex_highlighting = {'org'},
    },
    ensure_installed = {'cpp', 'c', 'lua', 'vim', 'vimdoc', 'yaml', 'python', 'make', 'cmake'},
}

return options
