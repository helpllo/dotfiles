local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

local plugins = {
    -- Eyecandies
    { "ellisonleao/gruvbox.nvim",
      priority = 1000,
      lazy = false,
      config = function()
        require("gruvbox").setup()
      end
    },
    -- ❤️ ❤️ 😯 🗄️
    { "goolord/alpha-nvim", 
      dependencies={"nvim-tree/nvim-web-devicons"},
      lazy = false,
      config = function()
        require("plugins.configs.alpha")
      end,
    },
    { "nvim-lualine/lualine.nvim", 
      dependencies={"nvim-tree/nvim-web-devicons"},
      lazy=false,
      opts = function()
         return require("plugins.configs.lualine")
      end,
      config = function(_, opts)
        require("lualine").setup(opts) 
      end
    },
    -- Coding utilies
    { "numToStr/Comment.nvim",
      config = true,
      keys = {
       { "gcc", mode = "n", desc = "Comment toggle current line" },
       { "gc", mode = { "n", "o" }, desc = "Comment toggle linewise" },
       { "gc", mode = "x", desc = "Comment toggle linewise (visual)" },
       { "gbc", mode = "n", desc = "Comment toggle current block" },
       { "gb", mode = { "n", "o" }, desc = "Comment toggle blockwise" },
       { "gb", mode = "x", desc = "Comment toggle blockwise (visual)" },
      },
    },
    { 'akinsho/toggleterm.nvim', 
      version = "2.9.x",
      cmd = "ToggleTerm",
      config = true,
    },
    { "windwp/nvim-autopairs", 
      event = "InsertEnter", 
      config = true,
    },
    { "neovim/nvim-lspconfig",
      dependencies={
        "hrsh7th/nvim-cmp",
        "hrsh7th/cmp-nvim-lsp",
        "saadparwaiz1/cmp_luasnip",
        "L3MON4D3/LuaSnip"
      },
      init = function()
        require("utils").lazy_load "nvim-lspconfig" 
      end,
      config = function()
        --require("plugins.configs.lsp")
        require("lsp")
      end
    },
    { "nvim-treesitter/nvim-treesitter",
      build = ":TSUpdate",
      init = function()
        require("utils").lazy_load "nvim-treesitter"
      end,
      opts = function()
        return require("plugins.configs.treesitter")
      end,
      config = function(_, opts)
        require("nvim-treesitter.configs").setup(opts)
      end
    },
    { "TimUntersberger/neogit", 
      dependencies={"nvim-lua/plenary.nvim"},
      cmd = "Neogit",
      opts = function()
        return require("plugins.configs.neogit")
      end,
      config = function(_, opts)
        require("neogit").setup(opts)
      end
    },
    -- General utilies
    { "nvim-telescope/telescope.nvim", 
      dependencies={"nvim-lua/plenary.nvim"},
      cmd = "Telescope",
      version = "0.1.x",
      opts = function()
        return require("plugins.configs.telescope")
      end,
      config = function(_, opts)
        local telescope = require("telescope")
        telescope.setup(opts)
        telescope.load_extension("file_browser")
      end
    },
    { "nvim-telescope/telescope-file-browser.nvim",
      dependencies={"nvim-telescope/telescope.nvim"}
    },
    { "nvim-orgmode/orgmode", 
      dependencies={"nvim-treesitter/nvim-treesitter"},
      ft = {"org"},
      opts = function()
        return require("plugins.configs.orgmode")
      end,
      config = function(_, opts)
          local orgmode = require("orgmode")
          orgmode.setup_ts_grammar()
          orgmode.setup(opts)
      end
    },
    { "airblade/vim-rooter",
      lazy=false
    },
    "tenxsoydev/size-matters.nvim",
}

local options = {
    defaults = {
        lazy = true,
    },
    install = {
        colorscheme = { "gruvbox" },
    }
}

require("lazy").setup(plugins,options)
--[[
    -- C++ highlight
    use 'bfrg/vim-cpp-modern'
    use 'lambdalisue/suda.vim'
    -- Rooter
    use 'airblade/vim-rooter'
    -- omnisharp-vim for good c# support
    use 'OmniSharp/omnisharp-vim'
end)]]
