-- Orginal Config is in xmonad_org.hs
import XMonad
import Data.Monoid
import System.Exit
-- Actions
import XMonad.Actions.CycleWS
import XMonad.Actions.Commands
-- Hooks
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.EwmhDesktops
-- Utils
import XMonad.Util.Run
import XMonad.Util.SpawnOnce
import XMonad.Util.EZConfig
-- Prompts
import XMonad.Prompt.ConfirmPrompt
import XMonad.Prompt.Shell
import XMonad.Prompt.Pass
import XMonad.Prompt
-- Layouts
import XMonad.Layout.Grid
import XMonad.Layout.NoBorders
import XMonad.Layout.Spacing
import XMonad.Layout.ShowWName

import qualified XMonad.StackSet as W
import qualified Data.Map        as M
-- Other
import Data.Char

-- Colors
myBackgroudColor = "#282828"
myFontColor = "#fbf1c7"
myXmobarTitleColor = "#959595"
myXmobarCourrentWorkspaceColor = "#bdae93"
myNormalBorderColor  = "#928374"
myFocusedBorderColor = "#cc241d"

-- Vars 
myTerminal = "alacritty "
myTextEditor = "emacsclient -a emacs "
myEmacs = "emacsclient -c -a emacs "
myFont = "xft:Roboto:size=9:bold:antialias=true:hinting=true"
myBrowser = "librewolf"
myBorderWidth = 1
-- Mod Mask(s)
myModMask = mod4Mask
sessionManager = "systemctl"
-- Workspaces
myWorkspaces = ["web","dev","cmd","games","media","vm","txt","gfx","other","background"]

myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

myClickJustFocuses :: Bool
myClickJustFocuses = False
-- Prompts Config(s)
promptConfig = def { 
    font        = myFont
  , borderColor = "#1e2320"
  , fgColor     = myFontColor
  , fgHLight    = "#ffffff"
  , bgColor     = myBackgroudColor
  , bgHLight    = "#3a3a3a"
  , height      = 32
  , position    = CenteredAt {xpCenterY = 0.15, xpWidth = 0.52}
  }
-- ShowWName config
myShowWNameTheme = def 
	{ swn_font	= "xft:Roboto:bold:size=40"
	, swn_fade	= 1.0
	, swn_bgcolor	= myBackgroudColor
	, swn_color	= myFontColor
	}

switchWorkspace :: Int -> Bool -> X()
switchWorkspace x z = do
    if z == True then runCommand' $ "shift" ++ ['"'] ++ myWorkspaces !! (x-1) ++ ['"']
    else runCommand' $ "view" ++ ['"'] ++ myWorkspaces !! (x-1) ++ ['"']

myKeys = \c -> mkKeymap c $ [
 --- Basic
   ("M4-q", spawn "xmonad --recompile; xmonad --restart")
 , ("M4-<Return>", spawn myTerminal)
 , ("M4-d s", spawn "rofi -show run")
 , ("M4-d d", spawn "rofi -show drun")
 , ("M4-S-q", kill)
 --- Layout control
 , ("M4-<Space>", sendMessage NextLayout)
 , ("M4-S-<Space>", sendMessage FirstLayout)
 , ("M4-t", withFocused $ windows . W.sink)
 , ("M4-b", sendMessage ToggleStruts)
 , ("M4-h", sendMessage Shrink)
 , ("M4-l", sendMessage Expand)
 , ("M4-,", sendMessage (IncMasterN 1))
 , ("M4-.", sendMessage (IncMasterN (-1)))
 , ("M4-j", windows W.focusDown)
 , ("M4-k", windows W.focusUp)
 --- Session control
 , ("M4-e a", confirmPrompt promptConfig "logout" $ io (exitWith ExitSuccess))
 , ("M4-e s", confirmPrompt promptConfig "suspend" $ spawn (sessionManager ++ " suspend"))
 , ("M4-e d", confirmPrompt promptConfig "restart" $ spawn (sessionManager ++ " reboot"))
 , ("M4-e f", confirmPrompt promptConfig "poweroff" $ spawn (sessionManager ++ " poweroff"))
 --- Multimedia
 , ("<XF86AudioMute>", spawn "amixer -n set Master toggle")
 , ("<XF86AudioLowerVolume>", spawn "amixer -n set Master 5%-")
 , ("<XF86AudioRaiseVolume>", spawn "amixer -n set Master 5%+")
 -- MPD control
 , ("<XF86AudioPlay>", spawn "playerctl play-pause")
 , ("<XF86AudioPrev>", spawn "playerctl previous")
 , ("<XF86AudioNext>", spawn "playerctl next")
 --- Workspaces
 , ("M4-1", switchWorkspace 1 False)
 , ("M4-2", switchWorkspace 2 False)
 , ("M4-3", switchWorkspace 3 False)
 , ("M4-4", switchWorkspace 4 False)
 , ("M4-5", switchWorkspace 5 False)
 , ("M4-6", switchWorkspace 6 False)
 , ("M4-7", switchWorkspace 7 False)
 , ("M4-8", switchWorkspace 8 False)
 , ("M4-9", switchWorkspace 9 False)
 , ("M4-0", switchWorkspace 10 False)
 , ("M4-S-1", switchWorkspace 1 True)
 , ("M4-S-2", switchWorkspace 2 True)
 , ("M4-S-3", switchWorkspace 3 True)
 , ("M4-S-4", switchWorkspace 4 True)
 , ("M4-S-5", switchWorkspace 5 True)
 , ("M4-S-6", switchWorkspace 6 True)
 , ("M4-S-7", switchWorkspace 7 True)
 , ("M4-S-8", switchWorkspace 8 True)
 , ("M4-S-9", switchWorkspace 9 True)
 , ("M4-S-0", switchWorkspace 10 True)
 --- Test
  ]


------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
--
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))
    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]

------------------------------------------------------------------------
-- Layouts:

-- You can specify and transform your layouts by modifying these values.
-- If you change layout bindings be sure to use 'mod-shift-space' after
-- restarting (with 'mod-q') to reset your layout state to the new
-- defaults, as xmonad preserves your old layout settings by default.
--
-- The available layouts.  Note that each layout is separated by |||,
-- which denotes layout choice.
--
myLayout = avoidStruts ( tiled ||| Mirror tiled ||| Grid ||| noBorders Full)
  where
     -- default tiling algorithm partitions the screen into two panes
     tiled   = Tall nmaster delta ratio

     -- The default number of windows in the master pane
     nmaster = 1

     -- Default proportion of screen occupied by master pane
     ratio   = 1/2

     -- Percent of screen to increment by when resizing panes
     delta   = 3/100

------------------------------------------------------------------------
-- Window rules:

-- Execute arbitrary actions and WindowSet manipulations when managing
-- a new window. You can use this to, for example, always float a
-- particular program, or have a client always appear on a particular
-- workspace.
--
-- To find the property name associated with a program, use
-- > xprop | grep WM_CLASS
-- and click on the client you're interested in.
--
-- To match on the WM_NAME, you can use 'title' in the same way that
-- 'className' and 'resource' are used below.
--
myManageHook :: ManageHook
myManageHook = (composeAll . concat $
    [ [resource  =? r --> doIgnore		| r <- myIgnores ]
    , [className =? c --> doShift "1:web"	| c <- myWebs]
    , [className =? c --> doShift "7:txt"	| c <- myTxtEditors]
    , [className =? c --> doShift "4:games"	| c <- myGames]
    , [className =? c --> doShift "5:media" | c <- myMovieandSound]
    , [className =? c --> doShift "6:vm"	| c <- myVms]
    , [className =? c --> doShift "2:dev"	| c <- myDev]
    , [className =? c --> doShift "8:gfx"	| c <- myGfx]
    , [className =? c --> doShift "9:other"	| c <- myNotes]
    , [isFullscreen --> doFullFloat]
    , [title =? "Obraz w obrazie" --> doFloat]
    ])
    where
    	myMovieandSound = ["Deadbeef","deadbeef","io.github.celluloid_player.Celluloid","Mpv","Xfmpc","xfmpc"]
    	myIgnores = ["kdesktop","desktop_window"]
        myDev = ["Codeblocks","Geany"]
        myWebs = ["Waterfox"]
        myGfx = ["Gimp","Inkspace"]
        myTxtEditors = ["Gedit"]
        myVms = ["Virt-manager","Virtualbox"]
        myGames = ["Xonotic","Minecraft"]	
        myNotes = ["joplin","Joplin"]
-----------------------------------------------------------------------
-- Event handling

-- * EwmhDesktops users should change this to ewmhDesktopsEventHook
--
-- Defines a custom handler function for X Events. The function should
-- return (All True) if the default handler is to be run afterwards. To
-- combine event hooks use mappend or mconcat from Data.Monoid.
--
--myEventHook = mempty
--ewmhDesktopsEventHook = mempty
------------------------------------------------------------------------
-- Status bars and logging

-- Perform an arbitrary action on each internal state change or X event.
-- See the 'XMonad.Hooks.DynamicLog' extension for examples.
--
myLogHook = return ()
------------------------------------------------------------------------
-- Startup hook

-- Perform an arbitrary action each time xmonad starts or is restarted
-- with mod-q.  Used by, e.g., XMonad.Layout.PerWorkspace to initialize
-- per-workspace layout choices.
--
-- By default, do nothing.
myStartupHook = do
 -- Set keyboard layout
 spawn "setxkbmap pl"
 -- Polybar
 spawnOnce "polybar"
 -- My wallpaper
 spawn "nitrogen --restore"
 -- Polkit agent
 spawnOnce "/usr/lib/mate-polkit/polkit-mate-authentication-agent-1"
 -- MPD
 spawnOnce "mpd"
 -- Mount my second disk using udisks
 spawnOnce "udisksctl mount -b /dev/sdc1"
------------------------------------------------------------------------
-- Now run xmonad with all the defaults we set up.

-- Run xmonad with the settings you specify. No need to modify this.
 --xmproc <- spawnPipe "xmobar"
main = do
 xmonad $ docks $ ewmhFullscreen $ ewmh defaults -- {
        --logHook = dynamicLogWithPP $ xmobarPP {
        --    ppOutput = hPutStrLn xmproc
        --    , ppTitle = xmobarColor "#bdae93" "" . shorten 30
        --    , ppLayout = return ""
        --    , ppCurrent = xmobarColor myXmobarCourrentWorkspaceColor ""
        --    , ppSep = "  "
        --    }
        --}
defaults = def {
      -- simple stuff
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         = myWorkspaces,
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,

      -- key bindings
        keys               = myKeys,
        mouseBindings      = myMouseBindings,

      -- hooks, layouts
        manageHook = manageDocks <+> myManageHook,
        layoutHook         = showWName' myShowWNameTheme $ myLayout,
        handleEventHook    = mempty,
        startupHook        = myStartupHook,
        logHook            = myLogHook
        }
