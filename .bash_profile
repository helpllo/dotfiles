#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc
# Block of code for starting qtile if bash loaded in tty1
#if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
#    #qtile start -b wayland
#    #exec startx
#fi
# export QT_QPA_PLATFORMTHEME=qt5ct
. "$HOME/.cargo/env"

if [ -e /home/linuxmarek/.nix-profile/etc/profile.d/nix.sh ]; then . /home/linuxmarek/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer
