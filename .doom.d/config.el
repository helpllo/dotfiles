;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'gruvbox)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/Dokumenty/Notes")

;; Light-up the cursor
(beacon-mode 1)

(after! vterm
  (setq vterm-shell "/bin/zsh"))

;; Dired (copied from dt's config...)
(evil-define-key 'normal dired-mode-map
  (kbd "<left>") 'dired-up-directory
  (kbd "<right>") 'dired-open-file
  (kbd "m") 'dired-mark
  (kbd "t") 'dired-toggle-marks
  (kbd "u") 'dired-unmark
  (kbd "a") 'dired-unmark-all-files
  (kbd "C") 'dired-do-copy
  (kbd "D") 'dired-do-delete
  (kbd "J") 'dired-goto-file
  (kbd "M") 'dired-do-chmod
  (kbd "O") 'dired-do-chown
  (kbd "P") 'dired-do-print
  (kbd "R") 'dired-do-rename
  (kbd "T") 'dired-do-touch
  (kbd "Z") 'dired-do-compress
  (kbd "+") 'dired-create-directory
  (kbd "-") 'dired-do-kill-lines
  (kbd "n") 'dired-mark-extension
  (kbd "b") 'dired-mark-files-regexp)
(map! :leader
      :desc "Magit push" "g p" #'magit-push)

;; Org fonts setup (also stolen from dt's config)
;; (defun dt/org-colors-doom-one ()
;;   "Enable Doom One colors for Org headers."
;;   (interactive)
;;   (dolist
;;       (face
;;        '((org-level-1 1.7 "#51afef" normal)
;;          (org-level-2 1.6 "#c678dd" normal)
;;          (org-level-3 1.5 "#98be65" normal)
;;          (org-level-4 1.4 "#da8548" normal)
;;          (org-level-5 1.3 "#5699af" normal)
;;          (org-level-6 1.2 "#a9a1e1" normal)
;;          (org-level-7 1.1 "#46d9ff" normal)
;;          (org-level-8 1.0 "#ff6c6b" normal)))
;;     (set-face-attribute (nth 0 face) nil :font doom-font :weight 'normal :height (nth 1 face) :foreground (nth 2 face)))
;;     (set-face-attribute 'org-table nil :font doom-font :weight 'normal :height 1.0 :foreground "#bfafdf")
